const char client_private_key[] PROGMEM = R"EOF(
-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCucxUWXJRhKjt2
JMANHJ6bqUmfycEqyPp/DCo8sgAWOliiIvbigg2xlfvAHww4LgWHYc5K+z/uyd48
EXx/S9MCOqOReNOP1WvfwKo9l4sQNTTVCiuPFpjbUH2bAL1cbC66aCt3Ifh+Zf0W
7xh0nZRh2Ssebf5vvRe+9awwQsnwo79gu/mGA/GqpE3u7lSocD1NQGT0wzLj5VaV
jCMvswf9qelvFBkO9KAKs1Oi5sPu9t5/BfqUb1Ou/canmJRcD40mvgacM/udPbQ1
RUZTvjeDXdoK4W9X+IPU+PhXyYZq6ORphd0P1amsNEDNE2SjIXhD+BiGs++1Dvau
VkSlxTTTAgMBAAECggEATvgTjKreFFoMzr92HLle5zIr/ORUyCxwkMCOAinFtko1
Qzg2sHSVBFXTv7WOT4Qtxoo022Z/G3ZQXrq/s9Q8Md8fmkOq6YqAKarKpdULA4xC
XUocS8q76VO3eEpObh8ezA1J18UN+xHqDs6vMGMA/4ZSaZT7P9PGgxAtIS+qwH5w
NKN1kdIw27rKL1wTHzCq/gPWiaTjROxzf9V5Ujn9/jtcMIA8KAJ0WWvj9b5QBpuo
cKpWcHio16lCAjNj7vjA/KK5YpP19YXL3rFYxsZ8WuTBgvniqfiHM4zMBwnF7BOo
iLlB+6R2UZF9Hk721dlSkMW4XpTkXdxjLeAb405hYQKBgQDbmRTGLfsT2WKQvIV4
a1XW6oZmKhTbEqzaYIcCtNO6ZuF48m6UYDqfVr/2LKBurQcsAFNEdNggdLQc8CLY
Nh3GkT7ZUVwCDXOS/kMQzbjenwGTq3U1Aekp+b7jx5ytzgH+At6hwaXJ9sFKrFbO
1pmbv7dqJhO1AQDEq4rJkWZKkQKBgQDLXhH1TFGAIbwnBepnsz8FNRAOrHvsgdgE
QorF9o8wP4FJ6EYb7ntaLedkqW82DixIJ9A9Rja05Qf9VmM1A1yUIoH2xV/Gdun3
waIuIs0RJ6hwy7kveS3GmYz5qusk9vEYJza6T0MFr213pxUpazo4vmkSpSoqiwsb
FMIYJtxTIwKBgF+yhmsW/qPXyCxq+39Ox0mxSoCbNhuCN/GyvyNeyiYhT4D1pVrj
1Kg7lcwiBog0vztdqOvcP5NlSFiUDZtLeg5enZT277G1Svhz9aYNANODV5ySy7Ed
9A5m19lL0+uqKxQXDA5R5X3uGq4ADJR20Noe4j9P+KaYmU0btI/C3WEBAoGALMsM
g44KyYEksyevaKXndJsqbUD6jq5OySlq7Y2QHl1uebvqbU6K5uMDe32CXFKk2EPE
rype9FsZ9mfntA6IfqxXGeaBYPJiOErzENxjdKrag/WrVVSIi5zYm0lVP7AQjHRe
roy0w3TPVLJ9i8DtcwAlnpC4h+RWTuG0mqM8licCgYEA0aXdiYKxfNieYOQIsI3o
RNMeWJAkNPGrXA2EhyO6b0zzZ80oC/c4pTcNoJm/s3oyVVXbwm8fmWdZEWfMGKMA
ZrtRet/ebmnLBVLfYAO/KDmc8v6atDsFm63pcSCMTlBa5aaDYafZw14NsjwIw5yZ
6K1hZgKj4Mz0EbPYGKIzMw0=
-----END PRIVATE KEY-----
)EOF";

const char client_cert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDZTCCAk0CFCokTyiDi5NrZA1pmLUhV3u3n8SmMA0GCSqGSIb3DQEBCwUAMG0x
CzAJBgNVBAYTAklEMRIwEAYDVQQIDAlFYXN0IEphdmExDzANBgNVBAcMBk1hbGFu
ZzEUMBIGA1UECgwLSmF0aW5vbXhVTU0xCzAJBgNVBAsMAkNBMRYwFAYDVQQDDA0z
MS4xODcuNzUuMTgxMB4XDTIyMTEwNjEwMDAyN1oXDTIzMTEwNjEwMDAyN1owcTEL
MAkGA1UEBhMCSUQxEjAQBgNVBAgMCUVhc3QgSmF2YTEPMA0GA1UEBwwGQmxpdGFy
MRQwEgYDVQQKDAtKYXRpbm9teFVNTTEPMA0GA1UECwwGQ2xpZW50MRYwFAYDVQQD
DA0zMS4xODcuNzUuMTgxMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
rnMVFlyUYSo7diTADRyem6lJn8nBKsj6fwwqPLIAFjpYoiL24oINsZX7wB8MOC4F
h2HOSvs/7snePBF8f0vTAjqjkXjTj9Vr38CqPZeLEDU01QorjxaY21B9mwC9XGwu
umgrdyH4fmX9Fu8YdJ2UYdkrHm3+b70XvvWsMELJ8KO/YLv5hgPxqqRN7u5UqHA9
TUBk9MMy4+VWlYwjL7MH/anpbxQZDvSgCrNToubD7vbefwX6lG9Trv3Gp5iUXA+N
Jr4GnDP7nT20NUVGU743g13aCuFvV/iD1Pj4V8mGaujkaYXdD9WprDRAzRNkoyF4
Q/gYhrPvtQ72rlZEpcU00wIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCd/PNkXUV7
U63s1LPVxJSbJbmqCxdH81P6hxeNc3ngy21tegRj+zmhlZkwmmXuKyEhf1s3/o/c
1YtBYy2Uz5uldTK77sdiHG53ZwMuoEYvxiG1f+erhLs1v+2ONvqA9xcFS9qGXYEi
PdjO0cvWQxSc60KWWESerXOjFiQSRIS5I2H8AO5ugSsoyGbnverxoKl2r5xDj3KJ
FbWHWERK20DlgLvtrZCEH6PLVtYsAYfRAi5Tln2DWiap5XXhwQM2+SYA0sID/U36
5RvlmwTYSX4Qyvzl5xYFNxuhFEmRzlyjlR7Nca/B0L2Jsrsu9YHFs66zYd9bdcF5
0PFGov2vXSd+
-----END CERTIFICATE-----
)EOF";

static const char ca_cert[] PROGMEM = R"EOF(
-----BEGIN CERTIFICATE-----
MIIDuzCCAqOgAwIBAgIUZRPyH/AusVtK7clIe57z7MBpEjIwDQYJKoZIhvcNAQEL
BQAwbTELMAkGA1UEBhMCSUQxEjAQBgNVBAgMCUVhc3QgSmF2YTEPMA0GA1UEBwwG
TWFsYW5nMRQwEgYDVQQKDAtKYXRpbm9teFVNTTELMAkGA1UECwwCQ0ExFjAUBgNV
BAMMDTMxLjE4Ny43NS4xODEwHhcNMjIxMTA2MTAwMDI2WhcNMjMxMTA2MTAwMDI2
WjBtMQswCQYDVQQGEwJJRDESMBAGA1UECAwJRWFzdCBKYXZhMQ8wDQYDVQQHDAZN
YWxhbmcxFDASBgNVBAoMC0phdGlub214VU1NMQswCQYDVQQLDAJDQTEWMBQGA1UE
AwwNMzEuMTg3Ljc1LjE4MTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AMtLq9Ndyj/OD3rqVgS6M44olG6u8Uq6bziesgfYrRJEKbD2KmWP6fNVJr1J7XwK
ZMyPtJcw73MJuiXF0ghTocu6VeBxjBHjU531JlxM5cRANBTwTShn4eZg9nWs+5Xo
numfKf1itIbO+LLg28vdEIHkfagaMX598rMIfhe8HEGTd5pxLkI7uqV/8/f95VHy
aksrSFkhhgCP+dCjBL88lIHDEWNm+jyUDC6ZkFy89bXnwhEJaeHAR1t5jXxP6IZN
wFD5ZCwsudGckuwTW9FXgQylrk8KA83cFRevSssC0AiHoTlO9Rm4tn/mOpbJinxJ
y7sf6Eqe1mIFtBpwQr+9AZUCAwEAAaNTMFEwHQYDVR0OBBYEFFaEd8d8MgpJqLmM
u5qg277lYBGNMB8GA1UdIwQYMBaAFFaEd8d8MgpJqLmMu5qg277lYBGNMA8GA1Ud
EwEB/wQFMAMBAf8wDQYJKoZIhvcNAQELBQADggEBAMMEvXHgSssgUKgVHF0URdLG
kXAlI7+Iwh7mwRrX7uGfB7g+Oqez6HyNgVJN0JnAK8jqBSvh5Xr21srYViyvtRmW
uwTOSQoovhMCKT1bQ9azIk1gSt/cc0qh0TDSggEQ7Oq3qrszOxZA+jljWjZOMe+H
l1mL2L1m7FaVmndKlSITE4kqohM89OKe9TsThCtaGpcXlric/ExE0CU8Jmv/wng5
4G82JY0vX8ajrJbTH245fUJU67i36aWDmY5eWHhNSZAgTZBtSWXGF8/CyG1ibKvX
S2KhrSCLZRT6U+3d2Chb4ytKVUv4Ac7SEU23DUDvWTAR7RpkLv0k9/W3/O0VSCk=
-----END CERTIFICATE-----
)EOF";

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Update these with values suitable for your network.
const char* ssid = "....";
const char* password = "....";

// VPS/Server address
const char* mqtt_server = "31.187.75.181";

BearSSL::WiFiClientSecure espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
//sensor value
int value = 0;
//timestamp
char* times;

void setClock() {
  configTime(7*3600, 0 , "pool.ntp.org", "time.nis.gov");
  Serial.print("Syncing time");
  int i = 0;
  while (time(nullptr) < 1000000000ul && i<100) {
    Serial.print(".");
    delay(100);
    i++;
  }
  Serial.println();

  time_t tnow = time(nullptr);
  struct tm *timeinfo;
  char buffer [80];

  timeinfo = localtime (&tnow);
  strftime(buffer,80,"%Y-%m-%d %H:%M:%S",timeinfo);
  times = buffer;
  Serial.println(times);
}

void setup_wifi() {

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  char err_buf[256];
  
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe if available
      // client.subscribe("inTopic");
    } else {
      Serial.print("failed, rc=");
      Serial.println(client.state());
      espClient.getLastSSLError(err_buf, sizeof(err_buf));
      Serial.print("SSL error: ");
      Serial.println(err_buf);
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  BearSSL::X509List *serverTrustedCA = new BearSSL::X509List(ca_cert);
  BearSSL::X509List *serverCertList = new BearSSL::X509List(client_cert);
  BearSSL::PrivateKey *serverPrivKey = new BearSSL::PrivateKey(client_private_key);
  espClient.setTrustAnchors(serverTrustedCA);
  espClient.setClientRSACert(serverCertList, serverPrivKey);
  setup_wifi();
  setClock(); // Required for X.509 validation
  client.setServer(mqtt_server, 8883);
  client.setCallback(callback);
}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  unsigned long now = millis();
  //change the 2000 for the sending period
  if (now - lastMsg > 2000) {
    lastMsg = now;
    //value is the sensor value
    ++value;
    setClock();
    snprintf(msg, MSG_BUFFER_SIZE, "%ld,%s", value,times);
    Serial.print("Publish message: ");
    Serial.println(msg);
    // replace outTopic with the appropriate topic for sensor data
    // jatinom/eggs
    // jatinom/humidities
    // jatinom/temperatures
    // jatinom/water_flows
    client.publish("jatinom/humidities", msg);
    client.publish("jatinom/eggs", msg);
    client.publish("jatinom/temperatures", msg);
    client.publish("jatinom/water_flows", msg);
  }
}
