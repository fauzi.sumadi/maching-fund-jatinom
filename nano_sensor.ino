#include <Adafruit_AHTX0.h>
Adafruit_AHTX0 aht; //pin sensor suhu di I2C_A4 dan A5
#ifndef HAVE_HWSerial
#endif
#include <SoftwareSerial.h>
SoftwareSerial LCDHMI(2,3); //RX, TX 

const int Relay[5] = {4,5,6,7,8}; //relay 4 kipas_1 pompa
boolean relayState[5]= {1,1,1,1,1};
float kecepatan;
 
int co;     
float nh3,no2;

const int S_analog = 1023.0; //Modul Sensor Amonia

void setup() {
  LCDHMI.begin(115200);
  Serial.begin(115200);
  for(int i=0;i<=4;i++)
    pinMode(Relay[i],OUTPUT);
    relayLoop();
    
//  if (! aht.begin()) {
//    Serial.println("Could not find AHT? Check wiring");
//    while (1) delay(10);
//  }
    Serial.println("AHT10 or AHT20 found");
}

void suhu(){
  sensors_event_t humidity, temp;
  aht.getEvent(&humidity, &temp);// populate temp and humidity objects with fresh data

  LCDHMI.print("tc.txt=\"");  //tampilkan ke LCD (tc)
  LCDHMI.print(temp.temperature); //Data suhu
  LCDHMI.print("\"");
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);

  LCDHMI.print("th.txt=\"");
  LCDHMI.print(humidity.temperature);
  LCDHMI.print("\"");
  LCDHMI.write(0xff); 
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
}

void amonia(){
  co = map (analogRead(A3), 0, S_analog, 1, 1000); //Kalkulasi Karbon Monoksida
  nh3 = map (analogRead(A2), 0, S_analog, 1, 500); //Kalkulasi Amonia
  no2 = (map (analogRead(A1), 0, S_analog, 5, 1000)) / 100.0 ; //Kalkulasi Nitrogen dioksida

  LCDHMI.print("tnh3.txt=\"");  //Amonia
  LCDHMI.print(nh3);
  LCDHMI.print("\"");
  LCDHMI.write(0xff); 
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
  
  LCDHMI.print("tco.txt=\"");  //karbon monoksida
  LCDHMI.print(co);
  LCDHMI.print("\"");
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);

  LCDHMI.print("tno2.txt=\""); //Nitrogen dioksida
  LCDHMI.print(no2);
  LCDHMI.print("\"");
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
}

void angin(){
  int sensorValue = analogRead(A0);
  kecepatan = ((sensorValue*(5.0/1023.0)-1.13) *9-10);
  
  LCDHMI.print("nA.txt=\"");
  LCDHMI.print(kecepatan);
  LCDHMI.print("\"");
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
  LCDHMI.write(0xff);
}

void sendData(){
  suhu();
  amonia();
  angin();  
}

void readData(){
 if(LCDHMI.available()>0){
  char data = LCDHMI.read(); 
  if(data == 'A'){
    relayState[0] = false;  Serial.println("ralay 1 ON"); 
  }else if(data == 'a'){
    relayState[0] = true; Serial.println("ralay 1 OFF");
  }else if(data == 'B'){
    relayState[1] = false;  Serial.println("ralay 2 ON");
  }else if(data == 'b'){
    relayState[1] = true; Serial.println("ralay 2 OFF");
  }else if(data == 'C'){
    relayState[2] = false;  Serial.println("ralay 3 ON");
  }else if(data == 'c'){
    relayState[2] = true; Serial.println("ralay 3 OFF");
  }else if(data == 'D'){
    relayState[3] = false;  Serial.println("ralay 4 ON");
  }else if(data == 'd'){
    relayState[3] = true; Serial.println("ralay 4 OFF");
  }else if(data == 'E'){
    relayState[4] = false;  Serial.println("Pompa 5 ON");
  }else if(data == 'e'){
    relayState[4] = true; Serial.println("Pompa 5 OFF");
  }
 }
}

void relayLoop(){
  for(int i = 4; i <= 8; i++)
    digitalWrite(i,relayState[i-4]);
}

void loop() {
  static uint32_t pM;  
  if((millis() - pM) > 1000) { 
    pM = millis();
    sendData();
  }
  readData();
  relayLoop();
}
